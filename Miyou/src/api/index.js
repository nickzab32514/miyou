import axios from 'axios';
import {add} from 'react-native-reanimated';

// const host = 'http://10.80.124.116:8080';
// const host = 'http://192.168.1.109:8080';
const host = 'https://miyou-backend.jumpingcrab.com';
// const host = 'http://172.16.13.79:8080';

export function signup(
  username,
  password,
  email,
  firstName,
  lastName,
  phone,
  address,
) {
  const data = {
    username: username,
    password: password,
    email: email,
    firstName: firstName,
    lastName: lastName,
    phone: phone,
    address: address,
  };
  return axios.post(`${host}/signup`, data);
}
export function logout(token) {
  return axios.post(`${host}/logout`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function login(username, password) {
  return axios.post(`${host}/login`, {username, password});
}

export function showProfile(token) {
  return axios.get(`${host}/profile`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function AllPet() {
  return axios.get(`${host}/listpetAll`);
}
export function listAllPet(token) {
  return axios.get(`${host}/alllistpet`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export function listAllTip(token) {
  return axios.get(`${host}/alllisttip`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function createOnePet(data, token) {
  console.log('token', token);
  return axios.post(`${host}/listpet`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function createOneTip(data, token) {
  console.log('token', token);
  return axios.post(`${host}/listtip`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export function UpdateStatusPet(id, Status, token) {
  return axios.put(
    `${host}/listpet/${id}/Accept`,
    {Status},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
}
export function UpdateShelter(id, ShelterID, token) {
  return axios.put(
    `${host}/listpet/${id}/AcceptShelter`,
    {ShelterID},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
}
export function HistoryAdd(token) {
  return axios.get(`${host}/historylistpet`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function ShowHistoryAccept(token) {
  return axios.get(`${host}/historyAccept`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function Showhistorylisttip(token) {
  return axios.get(`${host}/historylisttip`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function DeletePet(id, token) {
  return axios.delete(`${host}/listpet/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function DeleteTip(id, token) {
  return axios.delete(`${host}/listTip/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function UpdateImage(image, token) {
  console.log('image', image);
  return axios.put(`${host}/listpet`, image, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function getAllComment(id, token) {
  return axios.get(`${host}/comment/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export function AddComment(id, topic, description, token) {
  console.log('token', token);
  console.log('des', description);
  console.log('id', id);
  return axios.post(
    `${host}/comment/${id}`,
    {topic, description},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
}
