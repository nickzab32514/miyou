import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  SafeAreaView,
  TextInput,
} from 'react-native';
import style from './styles';
import {Icon} from 'react-native-elements';
import {Button} from 'react-native-paper';
import {signup} from './../../../api';
export default function Register(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [address, setAddress] = useState('');

  const onSignup = () => {
    console.log('firstname==>', firstName);
    signup(username, password, email, firstName, lastName, phone, address)
      .then(response => {
        props.navigation.navigate('Login');
        console.log('Test data store data ', response.data);
      })
      .catch(error => {
        console.log('eror', error);
      });
  };

  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 56,
            backgroundColor: '#4D70EB',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Icon
              name="ios-arrow-dropleft"
              type="ionicon"
              color="#fff"
              style={{marginLeft: 32, marginTop: 32}}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            height: 160,
            backgroundColor: '#4D70EB',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            style={style.tinyLogo}
            source={require('../../../asset/logoDog.jpg')}
          />
        </View>
        <ScrollView style={{flex: 1}} contentContainerStyle={{flexGrow: 1}}>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <View
              style={{
                backgroundColor: '#92B0E9',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomLeftRadius: 96,
                borderBottomRightRadius: 96,
                height: 48,
                width: '100%',
                marginBottom: 32,
              }}>
              <Text style={style.headkaigaodown}>Sign up</Text>
            </View>
            <View style={style.flexrowL}>
              <Icon
                name="account-circle-outline"
                type="material-community"
                color="#886bff"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#886bff'}}>
                Username
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input username'}
                mode="Flat "
                label="Username"
                value={username}
                onChangeText={value => setUsername(value)}
              />
            </View>
            <View style={style.flexrowL}>
              <Icon
                name="key-outline"
                type="material-community"
                color="#886bff"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#886bff'}}>
                Password
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input password'}
                mode="Flat "
                label="Password"
                value={password}
                onChangeText={value => setPassword(value)}
              />
            </View>
            <View style={style.flexrowLL}>
              <Icon
                name="key-change"
                type="material-community"
                color="#886bff"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#886bff'}}>
                Email
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please confirm email again'}
                mode="Flat "
                label="checkpassword"
                value={email}
                onChangeText={value => setEmail(value)}
              />
            </View>

            <View style={style.rowsFullname}>
              <Icon
                name="clipboard-text-outline"
                type="material-community"
                color="#886bff"
                style={{marginRight: 8}}
              />
              <View style={style.flexrow} />
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  color: '#886bff',
                  marginRight: 16,
                }}>
                First name
              </Text>
              <Icon
                name="card-bulleted-outline"
                type="material-community"
                color="#886bff"
                style={{marginLeft: 24, marginRight: 8}}
              />
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  marginRight: 24,
                  color: '#886bff',
                }}>
                Last name
              </Text>
            </View>
            <View style={style.rowsFullname}>
              <TextInput
                style={style.inputFull}
                placeholder={'Input firstname'}
                mode="Flat "
                label="Firstname"
                value={firstName}
                onChangeText={value => setFirstName(value)}
              />
              <TextInput
                style={style.inputFull}
                placeholder={'Input lastname'}
                mode="Flat "
                label="Lastname"
                value={lastName}
                onChangeText={value => setLastName(value)}
              />
            </View>
            <View style={style.flexrowPh}>
              <Icon
                name="phone"
                type="material-community"
                color="#886bff"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#886bff'}}>
                Phone
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input phone'}
                mode="Flat "
                label="phone"
                value={phone}
                keyboardType="numeric"
                maxLength={10}
                onChangeText={value => setPhone(value)}
              />
            </View>
            <View style={style.flexrowAdd}>
              <Icon
                name="map-marker-radius"
                type="material-community"
                color="#886bff"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#886bff'}}>
                Address
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input address'}
                mode="Flat "
                label="Address"
                value={address}
                onChangeText={value => setAddress(value)}
              />
            </View>
          </View>

          <View style={style.flexrowS}>
            <Button
              style={style.buttonSubmit}
              mode="Contained"
              onPress={onSignup}>
              <Text style={style.whitefont}>Signup</Text>
            </Button>
          </View>
        </ScrollView>
        {/* </ImageBackground> */}
      </SafeAreaView>
    </View>
  );
}
