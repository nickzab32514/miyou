import React, {useState, useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import style from './styles';
import {TextInput, Button} from 'react-native-paper';
import {setToken} from '../../../action/Authaction';
import {login} from '../../../api/index';
import {useNavigation} from '@react-navigation/native';

export default function Login(props) {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const onlogin = () => {
    login(username, password)
      .then(response => {
        console.log('response  on token===>', response);
        console.log('Successful login -=-=-=-=-=-=>', response.data.token);
        dispatch(setToken(response.data.token));
        props.navigation.navigate('TopTab');
      })
      .catch(error => {
        console.log('response  on token===>', username);
        console.log('response  on token===>', password);

        console.log('eror', error);
        alert('incorrect input correct');
      });
  };
  useEffect(() => {
    const unsubcrible = navigation.addListener('focus', () => {
      setPassword('');
      setUsername('');
    });
    return unsubcrible;
  }, [navigation]);
  return (
    <ScrollView style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <ImageBackground
          source={require('../../../asset/bg6.jpg')}
          resizeMode="cover"
          style={style.sizeBg}>
          <View style={style.scoreBar} contentContainerStyle={{flexGrow: 1}}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Text style={style.headkaigaodown}>MI YOU</Text>
              <Image
                style={style.tinyLogo}
                source={require('../../../asset/logoDog.jpg')}
              />
              <View style={style.flexrow}>
                <TextInput
                  style={style.inputText}
                  placeholder={'Username'}
                  mode="Flat "
                  label="Username"
                  value={username}
                  onChangeText={value => setUsername(value)}
                />
              </View>
              <View style={style.flexrow}>
                <TextInput
                  style={style.inputText}
                  placeholder={'Password'}
                  mode="Flat "
                  secureTextEntry
                  label="Password"
                  value={password}
                  onChangeText={value => setPassword(value)}
                />
              </View>
            </View>
            <View style={style.flexrow}>
              <Text style={style.whitefont}>Don't have an account?</Text>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Register')}>
                <Text style={style.whiteSigup}>Signup</Text>
              </TouchableOpacity>
            </View>
            <View style={style.flexrowS}>
              <Button
                style={style.buttonSubmit}
                mode="Outlined "
                icon="login"
                onPress={onlogin}>
                {/* // onPress={() => clickSubmit()}> */}
                <Text style={style.whitefont}>Login</Text>
              </Button>
            </View>
          </View>
        </ImageBackground>
      </SafeAreaView>
    </ScrollView>
  );
}
