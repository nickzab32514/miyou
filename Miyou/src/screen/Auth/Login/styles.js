import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  icons: {
    margin: 5,
  },
  flexrowTab: {
    flex: 0,
    flexDirection: 'row',
    backgroundColor: '#FA9494',
  },
  sizeBg: {
    width: '100%',
    height: '100%',
  },
  whiteSigup: {
    color: '#fff',
    marginLeft: 5,
    marginTop: 5,
    fontSize: 18,
    fontWeight: 'bold',
  },
  whitefont: {
    marginTop: 8,
    marginLeft: 90,
    color: '#fff',
  },
  headkaigaodown: {
    fontSize: 32,
    fontWeight: '100',
    color: '#fff',
    marginTop: 40,
  },
  headresgister: {
    fontSize: 32,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 20,
  },
  flexrow: {
    flex: 0,
    flexDirection: 'row',
  },
  sizePage: {
    flex: 1,
  },
  inputText: {
    width: '70%',
    marginTop: 16,
    marginBottom: 20,
    borderLeftColor: '#886bff',
    borderLeftWidth: 17,
    borderRightWidth: 17,
    borderRadius: 10,
    borderRightColor: '#886bff',
    fontSize: 14,
  },
  buttonSubmit: {
    paddingTop: 2,
    marginTop: 64,

    backgroundColor: '#886bff',
    width: 200,
    height: 40,
    borderRadius: 100,
  },
  flexrowS: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tinyLogo: {
    width: '20%',
    height: '20%',
    borderRadius: 100,
  },
});
