import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';

import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  StatusBar,
  SafeAreaView,
  TextInput,
} from 'react-native';
import style from './styles';
import {Icon} from 'react-native-elements';
import {Showhistorylisttip, DeleteTip} from '../../../api';
import {Button, List} from 'react-native-paper';
import {useSelector} from 'react-redux';

const Data = [
  {
    title: 'Appoinssddatments',
    icon: 'flight-takeoff',
  },
  {
    title: 'dss',
    icon: 'flight-takeoff',
  },
];
export default function HistoryTip(props) {
  const [allListTip, setAllListTip] = useState([]);
  const token = useSelector(state => state.auth);
  const navigation = useNavigation();
  const fetchShowhistorylisttip = () => {
    Showhistorylisttip(token)
      .then(res => {
        setAllListTip(res.data);
        console.log('Test data store Tip ', res);
        console.log('data Tip store data=============== ', res.data);
        console.log('Tip tp=============== ', res.data.Topic);
        // console.log('Name Tip=============== ', res.data.Sex);
      })
      .catch(error => {
        if (error.res) {
          console.log('error', error);
        } else {
          console.log('connect Error!!!');
        }
      });
  };
  useEffect(() => {
    const unsubcrible = navigation.addListener('focus', () => {
      fetchShowhistorylisttip();
    });
    return unsubcrible;
  }, [navigation]);

  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 56,
            backgroundColor: '#fcfcfc',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Icon
              name="ios-arrow-dropleft"
              type="ionicon"
              color="#a17176"
              style={{marginLeft: 32, marginTop: 32}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 200,
            backgroundColor: '#fcfcfc',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ImageBackground
            source={require('../../../asset/run.jpg')}
            resizeMode="cover"
            style={style.sizeBg}
          />
        </View>
        <ScrollView
          style={{backgroundColor: '#f5e9ec', flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#c7bcb5',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomLeftRadius: 96,
                borderBottomRightRadius: 96,
                height: 48,
                width: '100%',
                marginBottom: 32,
                flexDirection: 'row',
              }}>
              <Text style={style.headkaigaodown}>History All tip</Text>
            </View>
            {allListTip.map((item, index) => (
              <View style={{flexDirection: 'row-reverse'}}>
                <List.Item
                  title={item.Topic}
                  style={{
                    backgroundColor: 'white',
                    borderColor: '#ad899d',
                    borderBottomWidth: 1,
                    width: 296,
                    marginBottom: 8,
                  }}
                  left={props => (
                    <Icon
                      name="folder"
                      type="material-community"
                      color="#ad899d"
                      style={{margin: 8}}
                    />
                  )}
                  onPress={() =>
                    props.navigation.navigate('InfoTip', {item, index})
                  }
                />
                {/* <TouchableOpacity onPress={DeleteTipone}>
                  <Icon
                    name="delete"
                    type="antdesign"
                    color="#a17176"
                    style={{
                      padding: 4,
                      backgroundColor: 'white',
                      borderRadius: 100,
                      marginTop: 16,
                    }}
                  />
                </TouchableOpacity> */}
              </View>
            ))}
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
