import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  StatusBar,
  SafeAreaView,
  TextInput,
} from 'react-native';
import style from './styles';
import {Icon} from 'react-native-elements';
import {Button} from 'react-native-paper';
import {useSelector} from 'react-redux';
import {createOneTip} from '../../../api';

export default function AddTip(props) {
  const token = useSelector(state => state.auth);

  const [topic, setTopic] = useState('');
  const [description, setDescription] = useState('');

  const addTip = () => {
    let data = {
      topic: topic,
      description: description,
    };
    console.log('topic==>', data);

    createOneTip(data, token)
      .then(response => {
        console.log('Test PEt--> ', response.data);
        // props.navigation.navigate('infoPet');
      })

      .catch(error => {
        console.log('eror', response);
      });
    props.navigation.navigate('TopTab');
  };

  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 56,
            backgroundColor: '#e4d2cb',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Icon
              name="ios-arrow-dropleft"
              type="ionicon"
              color="#a17176"
              style={{marginLeft: 32, marginTop: 32}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 200,
            backgroundColor: '#e4d2cb',
            alignItems: 'center',
            justifyContent: 'center',
            //   onPress={() => authPickerImg()}
          }}>
          <ImageBackground
            source={require('../../../asset/pink.jpg')}
            resizeMode="cover"
            style={style.sizeBg}
          />
        </View>
        <ScrollView
          style={{backgroundColor: '#fff', flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#c5999d',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomLeftRadius: 96,
                borderBottomRightRadius: 96,
                height: 48,
                width: '100%',
                marginBottom: 32,
              }}>
              <Text style={style.headkaigaodown}>AddTip</Text>
            </View>
            <View style={style.flexrowL}>
              <Icon
                name="lightbulb-on-outline"
                type="material-community"
                color="#f2aba5"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#f2aba5'}}>
                Topic
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input Topic'}
                mode="Flat "
                label="Topic"
                value={topic}
                onChangeText={value => setTopic(value)}
              />
            </View>
            <View style={style.flexrowLL}>
              <Icon
                name="file-document-edit-outline"
                type="material-community"
                color="#f2aba5"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#f2aba5'}}>
                Description
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input Description'}
                mode="Flat "
                label="Description"
                value={description}
                onChangeText={value => setDescription(value)}
              />
            </View>
          </View>
          <View style={style.flexrowS}>
            <Button
              style={style.buttonSubmit}
              mode="Contained"
              onPress={addTip}>
              <Text style={style.whitefont}>Submit</Text>
            </Button>
          </View>
          <Image
            style={style.tinyLogo}
            source={require('../../../asset/hello.gif')}
          />
        </ScrollView>
        {/* </ImageBackground> */}
      </SafeAreaView>
    </View>
  );
}
