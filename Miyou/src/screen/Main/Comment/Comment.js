import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, ScrollView} from 'react-native';
import {getAllComment, AddComment} from '../../../api';
import {Card, Title, TextInput, Button, IconButton} from 'react-native-paper';
import {useSelector} from 'react-redux';

export default function Comment(props) {
  const [allComment, setAllComment] = useState([]);
  const [comment, setComment] = useState('');
  const [topic, setTopic] = useState('');
  const token = useSelector(state => state.auth);

  const TipID = props.id;
  // const fetchComment = () => {
  //   getAllComment(props.id)
  //     .then(res => {
  //       setAllComment(res.data);
  //       console.log('suceess-->', res.data);
  //     })
  //     .catch(err => {
  //       console.log('err--->', err);
  //     });
  // };

  const handleComment = () => {
    AddComment(props.id, topic, comment, token)
      .then(res => {
        console.log('res-=-=-=-=', res);
      })
      .catch(err => {
        console.log('err : ', err);
      });
  };
  useEffect(() => {
    console.log('propsallComment', allComment);
    console.log('props;;;;;;', props.id);

    getAllComment(props.id, token)
      .then(res => {
        setAllComment(res.data);
        console.log('suceess-->', res.data);
      })
      .catch(err => {
        console.log('err--->', err);
      });
  }, [props.id]);
  return (
    <View>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <FlatList
          data={allComment}
          renderItem={({item}) => (
            <View style={{margin: 5}}>
              <View>
                <Card style={{width: '80%', marginLeft: 40}}>
                  <Card.Content>
                    <Title>{item.Topic}</Title>
                    <Title>{item.Description}</Title>
                  </Card.Content>
                </Card>
              </View>
            </View>
          )}
        />
      </ScrollView>
      {token !== null ? (
        <View
          style={{
            alignItems: 'center',
            margin: 10,
            flexDirection: 'column',
            backgroundColor: '#dab8ca',
            padding: 24,
            marginTop: 16,
            marginBottom: 16,
            borderRadius: 32,
            elevation: 8,
          }}>
          <Text style={{color: '#fff', fontSize: 24}}>Comment</Text>
          <Text style={{color: '#fff'}}>____________________________</Text>
          <TextInput
            style={{width: 250, marginVertical: 10, flex: 2}}
            label="topic"
            mode="outlined"
            selectionColor="pink"
            value={topic}
            onChangeText={value => setTopic(value)}
          />
          <TextInput
            style={{width: 250, marginVertical: 10, flex: 2}}
            label="Comment"
            mode="outlined"
            selectionColor="pink"
            value={comment}
            onChangeText={value => setComment(value)}
          />
          <Button
            style={{
              height: 50,
              borderWidth: 2,
              borderColor: '#bd2876',
              backgroundColor: '#fff',
            }}
            mode="contained"
            onPress={handleComment}>
            <Text style={{fontSize: 16, color: '#bd2876'}}>submit Comment</Text>
          </Button>
        </View>
      ) : (
        <View />
      )}
    </View>
  );
}
