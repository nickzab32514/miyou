import React, {useState, useEffect} from 'react';
import {
  View,
  ImageBackground,
  ScrollView,
  StatusBar,
  SafeAreaView,
  Text,
} from 'react-native';
// import {ListItem} from 'react-native-elements';
import {useSelector} from 'react-redux';

import style from './styles';
import {Segment, Tab, Tabs} from 'native-base';

import {HistoryAdd} from '../../../api';
import {Card, Paragraph, Button} from 'react-native-paper';
import {
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native-gesture-handler';
import {Icon} from 'react-native-elements';
// import {List} from 'react-native-paper';

const Data = [
  {
    title: 'Appointments',
    icon: 'flight-takeoff',
  },
  {
    title: 'Trips',
    icon: 'flight-takeoff',
  },
];

export default function HistoryListPet(props) {
  const [allListPet, setAllListPet] = useState([]);
  const token = useSelector(state => state.auth);

  // const renderItem = ({item}) => (
  //   <ListItem
  //     style={{backgroundColor: 'white'}}
  //     title={item.id}
  //     subtitle={item.title}
  //     leftAvatar={{source: {uri: item.avatar_url}}}
  //     bottomDivider
  //     chevron
  //   />
  // );
  const fetchHistPet = () => {
    HistoryAdd(token)
      .then(res => {
        setAllListPet(res.data);
        console.log('Test data store data ', res);
        console.log('data data store data=============== ', res.data);
        console.log('Name name=============== ', res.data.Name);
        console.log('Name name=============== ', res.data.Sex);
      })
      .catch(error => {
        if (error.res) {
          console.log('error', error);
        } else {
          console.log('connect Error!!!');
        }
      });
  };
  useEffect(() => {
    fetchHistPet();
  }, []);

  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 56,
            backgroundColor: '#fffdf1',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Icon
              name="ios-arrow-dropleft"
              type="ionicon"
              color="#a17176"
              style={{marginLeft: 32, marginTop: 32}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 250,
            backgroundColor: '#fffbf2',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ImageBackground
            source={require('../../../asset/stand.jpg')}
            resizeMode="cover"
            style={style.sizeBg}
          />
        </View>
        <ScrollView
          style={{backgroundColor: '#f5e9ec', flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <View
            style={{
              backgroundColor: '#a18492',
              justifyContent: 'center',
              alignItems: 'center',
              borderBottomLeftRadius: 96,
              borderBottomRightRadius: 96,
              height: 48,
              width: '100%',
              marginBottom: 32,
            }}>
            <Text style={style.headkaigaodown}>History add pet</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 8}}>
            <Text
              style={{
                borderBottomWidth: 5,
                width: 150,
                paddingLeft: 16,
                color: '#6f4b4b',
                borderColor: '#6f4b4b',
              }}>
              History add pet
            </Text>
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{width: 300, margin: 8}}>
              {allListPet.map((item, index) => (
                <View>
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate('InfoDelete', {item, index})
                    }>
                    <Card style={{margin: 8, elevation: 4, height: 250}}>
                      <Card.Title title={item.Name} />
                      <Card.Content>
                        <View
                          style={{flexDirection: 'column', marginBottom: 8}}>
                          <View style={{flexDirection: 'row'}}>
                            <Paragraph style={{color: '#aba8ab'}}>
                              Gene :
                            </Paragraph>
                            <Paragraph style={{color: '#aba8ab'}}>
                              {item.Gene}
                            </Paragraph>
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <Paragraph style={{color: '#aba8ab'}}>
                              Age:
                            </Paragraph>
                            <Paragraph
                              style={{color: '#aba8ab', marginRight: 8}}>
                              {item.Age}
                            </Paragraph>
                            {item.Status === true ? (
                              <Button
                                style={style.Tags}
                                mode="Contained"
                                onPress={() =>
                                  props.navigation.navigate('AddPet')
                                }>
                                <Text style={style.fnTag}>Accept</Text>
                              </Button>
                            ) : (
                              <Button
                                style={style.TagsN}
                                mode="Contained"
                                onPress={() =>
                                  props.navigation.navigate('AddPet')
                                }>
                                <Text style={style.fnTagN}>Not Accept</Text>
                              </Button>
                            )}
                          </View>
                        </View>
                      </Card.Content>
                      <Card.Cover source={{uri: item.Image}} />
                      {/* style={{flexDirection: 'row', justifyContent: 'center'}}> */}
                    </Card>
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
