import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';
import Comments from '../Comment/Comment';

import {Segment, Tab, Tabs} from 'native-base';

import style from './styles';
import {Icon} from 'react-native-elements';
import {Button, List} from 'react-native-paper';

export default function InfoTip(props) {
  const listTip = props.route.params.item;
  const listTipID = props.route.params.item.ID;
  const [contentlistTip, setContentlistTip] = useState([]);

  useEffect(() => {
    setContentlistTip(listTip);
  }, []);

  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 56,
            backgroundColor: '#fffdf1',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Icon
              name="ios-arrow-dropleft"
              type="ionicon"
              color="#a17176"
              style={{marginLeft: 32, marginTop: 32}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 256,
            backgroundColor: '#fffdf1',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ImageBackground
            source={require('../../../asset/read.png')}
            resizeMode="cover"
            style={style.sizeBg}
          />
        </View>
        <ScrollView
          style={{backgroundColor: '#e3d3cc', flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#c5999d',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomLeftRadius: 96,
                borderBottomRightRadius: 96,
                height: 48,
                width: '100%',
                marginBottom: 32,
              }}>
              <Text style={style.headkaigaodown}>{listTip.Topic}</Text>
            </View>

            <View
              style={{
                alignItems: 'flex-start',
                justifyContent: 'center',
                backgroundColor: '#fff',
                padding: 32,
                marginBottom: 24,
                elevation: 2,
                borderRadius: 20,
                margin: 8,
                width: 300,
              }}>
              <View style={style.flexrowAdd}>
                <Icon
                  name="map-marker-radius"
                  type="material-community"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}>Description:</Text>
              </View>
              <View style={style.flexrow}>
                <Text style={style.headTop}>{listTip.Description}</Text>
              </View>
              {/* <TextInput
                style={style.inputText}
                placeholder={'Please input Topic'}
                mode="Flat "
                label="Topic"
                value={topic}
                onChangeText={value => setTopic(value)}
              /> */}
            </View>
          </View>
          <Comments navigation={props.navigation} id={listTip.ID} />
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
