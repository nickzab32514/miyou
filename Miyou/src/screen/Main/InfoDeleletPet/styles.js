import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  icons: {
    margin: 5,
  },
  flexrowTab: {
    flex: 0,
    flexDirection: 'row',
    backgroundColor: '#FA9494',
  },
  sizeBg: {
    width: '60%',
    height: '70%',
    marginLeft: 40,
    backgroundColor: '#e5b184',
    borderRadius: 100,
  },
  whiteSigup: {
    color: '#fff',
    marginLeft: 5,
    marginTop: 5,
    fontSize: 18,
    fontWeight: 'bold',
  },
  whitefont: {
    marginTop: 8,
    marginLeft: 90,
    color: '#fff',
  },
  headkaigaodown: {
    fontSize: 24,
    fontWeight: '100',
    color: '#fff',
    padding: 8,
  },
  headresgister: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 20,
  },
  flexrow: {
    flex: 0,
    flexDirection: 'row',
    marginBottom: 16,
  },
  flexrowLL: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginRight: 64,
    marginBottom: 16,
  },
  flexrowSex: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginRight: 156,
    marginBottom: 16,
  },
  flexrowConNum: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginRight: 64,
    marginBottom: 16,
  },
  flexrowDes: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginRight: 64,
    marginBottom: 16,
  },
  flexrowL: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginRight: 140,
    marginBottom: 16,
  },
  flexrowPh: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginRight: 156,
    marginBottom: 16,
  },
  flexrowAdd: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginRight: 132,
    marginBottom: 16,
  },
  flexrowAddMap: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginRight: 164,
    marginBottom: 16,
  },
  rowsFullname: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 40,
    marginBottom: 16,
  },
  sizePage: {
    flex: 1,
  },
  veryTp: {fontSize: 16, fontWeight: 'bold', color: '#a17176'},
  headTop: {
    fontSize: 16,
    marginBottom: 8,
    marginLeft: 8,
    fontWeight: '800',
    color: '#a17176',
  },
  buttonSubmit: {
    paddingTop: 2,
    marginTop: 24,
    marginBottom: 48,

    backgroundColor: '#f1aaa4',
    width: 280,
    height: 40,
    borderRadius: 100,
  },
  fontVivo: {
    color: '#597fbf',
    paddingBottom: 8,
  },
  buttonAddmap: {
    paddingTop: 2,
    marginTop: 24,
    marginBottom: 48,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#597fbf',
    width: 248,
    height: 36,
    borderRadius: 100,
  },
  flexrowS: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tinyLogo: {
    width: 96,
    height: 96,
    borderRadius: 100,
  },
  scoreBar: {
    flex: 1,
    marginBottom: 1,
  },
  textNotAccept: {
    backgroundColor: '#fff',
    paddingLeft: 32,
    paddingRight: 32,
    paddingTop: 8,
    paddingBottom: 8,
    marginTop: 16,
    borderColor: '#4e5cbf',
    borderWidth: 1,
    color: '#4e5cbf',
    borderRadius: 100,
  },
  textAccept: {
    backgroundColor: '#fff',
    paddingLeft: 32,
    paddingRight: 32,
    paddingTop: 8,
    paddingBottom: 8,
    marginTop: 16,
    borderColor: '#f2aba5',
    borderWidth: 1,
    color: '#f2aba5',
    borderRadius: 100,
  },
  textNot: {
    backgroundColor: 'red',
    paddingLeft: 32,
    paddingRight: 32,
    paddingTop: 8,
    paddingBottom: 8,
    marginTop: 16,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 100,
  },
});
