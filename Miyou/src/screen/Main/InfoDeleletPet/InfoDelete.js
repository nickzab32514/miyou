import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Segment, Tab, Tabs} from 'native-base';
import {UpdateStatusPet, UpdateShelter, DeletePet} from '../../../api';
import style from './styles';
import {Icon} from 'react-native-elements';
import {Button, List} from 'react-native-paper';
import {useSelector} from 'react-redux';

export default function InfoDelete(props) {
  const token = useSelector(state => state.auth);

  const listPet = props.route.params.item;
  const listPetID = props.route.params.item.ID;
  const ShelterID = props.route.params.item.ID;
  const status = props.route.params.item.Status;

  useEffect(() => {
    console.log('item--->', props.route.params.item);
    console.log('ID----->', props.route.params.item.Image);
  }, []);
  const DeletePetList = () => {
    DeletePet(listPetID, token).then(res => {
      console.log('res===>', res).catch(error => {
        props.navigation.navigate('TopTab');
        console.log('error', error);
      });
    });
  };
  const changeStatus = () => {
    UpdateStatusPet(listPetID, true, token)
      .then(res => {
        console.log('res===>', res);
        UpdateShelter(listPetID, listPetID, token)
          .then(res => {
            console.log('res===>', res);
            props.navigation.navigate('TopTab');
            alert('Accept sucessful');
          })
          .catch(error => {
            if (error.res) {
              console.log('error', error);
            } else {
              console.log('connect Error!!!', error);
            }
          });
      })
      .catch(error => {
        if (error.res) {
          console.log('error', error);
        } else {
          console.log('connect Error!!!', error);
        }
      });
  };
  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 56,
            backgroundColor: '#fff',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity onPress={() => props.navigation.navigate('TopTab')}>
            <Icon
              name="ios-arrow-dropleft"
              type="ionicon"
              color="#a17176"
              style={{marginLeft: 32, marginTop: 32}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 300,
            backgroundColor: '#fff',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={{uri: props.route.params.item.Image}}
            resizeMode="cover"
            style={style.sizeBg}
          />
          {listPet.Status == !true ? (
            <View style={style.flexrow}>
              <Text style={style.textNotAccept}>Not Accept</Text>
            </View>
          ) : (
            <View style={style.flexrow}>
              <Text style={style.textAccept}>Accept</Text>
            </View>
          )}
        </View>
        <ScrollView
          style={{backgroundColor: '#e3d3cc', flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#c5999d',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomLeftRadius: 96,
                borderBottomRightRadius: 96,
                height: 48,
                width: '100%',
                marginBottom: 32,
              }}>
              <Tabs
                tabStyle={{
                  backgroundColor: 'red',
                  color: '#fff',
                  height: 500,
                }}>
                <Tab
                  heading="Infomation"
                  tabStyle={{backgroundColor: '#a17176'}}
                  textStyle={{color: '#fff'}}
                  activeTabStyle={{backgroundColor: '#a17176'}}
                  activeTextStyle={{color: '#fff', fontWeight: 'normal'}}
                />
                <Tab
                  heading="Map"
                  tabStyle={{backgroundColor: '#a17176'}}
                  textStyle={{color: '#fff'}}
                  activeTabStyle={{backgroundColor: '#a17176'}}
                  activeTextStyle={{color: '#fff', fontWeight: 'normal'}}
                />
              </Tabs>
            </View>
            <View
              style={{
                alignItems: 'flex-start',
                justifyContent: 'center',
                backgroundColor: '#fff',
                padding: 32,
                marginBottom: 24,
                elevation: 2,
                borderRadius: 20,
                margin: 8,
                width: 280,
              }}>
              <View style={style.flexrowL}>
                <Icon
                  name="account-circle-outline"
                  type="material-community"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}>Name:</Text>
                <Text style={style.headTop}> {listPet.Name}</Text>
              </View>
              <View style={style.flexrowL}>
                <Icon
                  name="dna"
                  type="fontisto"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}> Gene:</Text>
                <Text style={style.headTop}> {listPet.Gene}</Text>
              </View>
              <View style={style.flexrowSex}>
                <Icon
                  name="intersex"
                  type="fontisto"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}> Sex :</Text>
                <Text style={style.headTop}>{listPet.Sex} </Text>
              </View>
              <View style={style.flexrowPh}>
                <Icon
                  name="hourglass-start"
                  type="fontisto"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}>Age:</Text>
                <Text style={style.headTop}> {listPet.Age}</Text>
              </View>
              <View style={style.flexrowDes}>
                <Icon
                  name="map-marker-radius"
                  type="material-community"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}>Description style:</Text>
              </View>
              <View style={style.flexrow}>
                <Text style={style.headTop}>{listPet.Description}</Text>
              </View>
              <View style={style.flexrowL}>
                <Icon
                  name="hubspot"
                  type="material-community"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}>Disease:</Text>
              </View>
              <View style={style.flexrow}>
                <Text style={style.headTop}>{listPet.Desease}</Text>
              </View>
              <View style={style.flexrowConNum}>
                <Icon
                  name="phone-classic"
                  type="material-community"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}>Contract number:</Text>
                <Text style={style.headTop}>{listPet.ContractNumber}</Text>
              </View>
              <View style={style.flexrowAdd}>
                <Icon
                  name="map-marker-radius"
                  type="material-community"
                  color="#a17176"
                  style={{marginRight: 8}}
                />
                <Text style={style.veryTp}>Address:</Text>
              </View>
              <View style={style.flexrow}>
                <Text style={style.headTop}>{listPet.Address}</Text>
              </View>
            </View>
          </View>
          <TouchableOpacity onPress={DeletePetList}>
            <View
              style={{
                margin: 32,
                justifyContent: 'flex-end',
                alignContent: 'flex-end',
                flexDirection: 'row',
              }}>
              <Icon
                name="delete-forever"
                type="materialicons"
                color="#c28395"
                reverse
                style={{marginRight: 8, marginBottom: 16}}
              />
            </View>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
