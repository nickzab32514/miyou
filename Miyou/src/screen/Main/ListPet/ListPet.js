import React, {useState, useEffect, Component} from 'react';
import {useNavigation} from '@react-navigation/native';

import {
  View,
  ImageBackground,
  ScrollView,
  StatusBar,
  SafeAreaView,
  Text,
  TextInput,
} from 'react-native';
// import {ListItem} from 'react-native-elements';
import {useSelector} from 'react-redux';

import style from './styles';
import {Container, Tab, Tabs, Content} from 'native-base';
import {listAllPet} from '../../../api';
import {Card, Paragraph, Button} from 'react-native-paper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Icon} from 'react-native-elements';
// import {List} from 'react-native-paper';

const Data = [
  {
    title: 'Appointments',
    icon: 'flight-takeoff',
  },
  {
    title: 'Trips',
    icon: 'flight-takeoff',
  },
];

export default function ListPet(props) {
  const [allListPet, setAllListPet] = useState([]);
  const token = useSelector(state => state.auth);
  const [searchList, setSearchList] = useState('');
  const navigation = useNavigation();

  const fetchListPet = () => {
    listAllPet(token)
      .then(res => {
        setAllListPet(res.data);
        console.log('Test data store data ', res);
        console.log('data data store data=============== ', res.data);
        console.log('Name name=============== ', res.data.Name);
        console.log('Name name=============== ', res.data.Sex);
      })
      .catch(error => {
        if (error.res) {
          console.log('error', error);
        } else {
          console.log('connect Error!!!');
        }
      });
  };

  useEffect(() => {
    const unsubcrible = navigation.addListener('focus', () => {
      fetchListPet();
    });
    return unsubcrible;
  }, [navigation]);
  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 200,
            backgroundColor: '#fff',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ImageBackground
            source={require('../../../asset/doglist.jpg')}
            resizeMode="cover"
            style={style.sizeBg}
          />
        </View>
        <View
          style={{
            backgroundColor: '#c5999d',
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomLeftRadius: 96,
            borderBottomRightRadius: 96,
            height: 48,
            width: '100%',
          }}>
          <Container>
            <Content>
              <Tabs
                tabStyle={{
                  backgroundColor: 'red',
                  color: '#fff',
                  height: 20,
                }}>
                <Tab
                  heading="All List Pet"
                  tabStyle={{backgroundColor: '#a17176'}}
                  textStyle={{color: '#fff'}}
                  activeTabStyle={{backgroundColor: '#a17176'}}
                  activeTextStyle={{color: '#fff', fontWeight: 'normal'}}
                />
              </Tabs>
            </Content>
          </Container>
        </View>
        <ScrollView
          style={{backgroundColor: '#e3d3cc', flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <View style={{flexDirection: 'row', marginTop: 8}}>
            <Text
              style={{
                borderBottomWidth: 5,
                width: 150,
                paddingLeft: 16,
                color: '#6f4b4b',
                borderColor: '#6f4b4b',
              }}>
              List of all pet
            </Text>
            <TouchableOpacity
              style={{marginLeft: 100}}
              onPress={() => props.navigation.navigate('Allmap')}>
              <Icon
                name="map-signs"
                type="font-awesome"
                color="#a17176"
                reverse
                size={16}
                style={{marginRight: 8}}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{marginLeft: 8}}
              onPress={() => props.navigation.navigate('AddPet')}>
              <Icon
                name="ios-add-circle"
                type="ionicon"
                reverse
                color="#a17176"
                size={16}
                style={{marginRight: 8}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{width: 300, margin: 8}}>
              {allListPet.map((item, index) => (
                <TouchableOpacity
                  onPress={() =>
                    props.navigation.navigate('InfoPet', {item, index})
                  }>
                  <Card style={{margin: 8, elevation: 4}}>
                    <Card.Title title={item.Name} />
                    <Card.Content>
                      <View style={{flexDirection: 'column', marginBottom: 8}}>
                        <View style={{flexDirection: 'row'}}>
                          <Paragraph style={{color: '#aba8ab'}}>
                            Gene :
                          </Paragraph>
                          <Paragraph style={{color: '#aba8ab'}}>
                            {item.Gene}
                          </Paragraph>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                          <Paragraph style={{color: '#aba8ab'}}>Age:</Paragraph>
                          <Paragraph style={{color: '#aba8ab', marginRight: 8}}>
                            {item.Age}
                          </Paragraph>
                          {item.Status === true ? (
                            <Button
                              style={style.Tags}
                              mode="Contained"
                              onPress={() =>
                                props.navigation.navigate('AddPet')
                              }>
                              <Text style={style.fnTag}>Accept</Text>
                            </Button>
                          ) : (
                            <Button
                              style={style.TagsN}
                              mode="Contained"
                              onPress={() =>
                                props.navigation.navigate('AddPet')
                              }>
                              <Text style={style.fnTagN}>Not Accept</Text>
                            </Button>
                          )}
                        </View>
                      </View>
                    </Card.Content>
                    <Card.Cover source={{uri: item.Image}} />
                  </Card>
                </TouchableOpacity>
              ))}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
