import React, {useState, useEffect} from 'react';
import {
  View,
  ImageBackground,
  ScrollView,
  StatusBar,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
// import {ListItem} from 'react-native-elements';

import style from './styles';
import {Segment, Text, Tab, Tabs} from 'native-base';

// import Tab3 from './tabThree';
import {Card, Paragraph, Button} from 'react-native-paper';
// import {Icon} from 'react-native-elements';
// import {List} from 'react-native-paper';

const Data = [
  {
    title: 'Appoinssddatments',
    icon: 'flight-takeoff',
  },
  {
    title: 'dss',
    icon: 'flight-takeoff',
  },
];

export default function All(props) {
  // const renderItem = ({item}) => (
  //   <ListItem
  //     style={{backgroundColor: 'white'}}
  //     title={item.id}
  //     subtitle={item.title}
  //     leftAvatar={{source: {uri: item.avatar_url}}}
  //     bottomDivider
  //     chevron
  //   />
  // );
  return (
    <ScrollView
      style={{backgroundColor: '#e3d3cc', flex: 1}}
      contentContainerStyle={{flexGrow: 1}}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={{width: 300, margin: 8}}>
          {Data.map((item, i) => (
            <TouchableOpacity
              onPress={() => props.navigation.navigate('Info Pet')}>
              <Card style={{margin: 8, elevation: 4}}>
                <Card.Title title={item.title} />
                <Card.Content>
                  <View style={{flexDirection: 'column', marginBottom: 8}}>
                    <View style={{flexDirection: 'row'}}>
                      <Paragraph style={{color: '#aba8ab'}}>Gene :</Paragraph>
                      <Paragraph style={{color: '#aba8ab'}}>
                        Card content
                      </Paragraph>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Paragraph style={{color: '#aba8ab'}}>Age :</Paragraph>
                      <Paragraph style={{color: '#aba8ab', marginRight: 8}}>
                        Card content
                      </Paragraph>
                      <Button
                        style={style.Tags}
                        mode="Contained"
                        onPress={() => props.navigation.navigate('AddPet')}>
                        <Text style={style.fnTag}>Accept</Text>
                      </Button>
                    </View>
                  </View>
                </Card.Content>
                <Card.Cover source={{uri: 'https://picsum.photos/700'}} />
              </Card>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </ScrollView>
  );
}
