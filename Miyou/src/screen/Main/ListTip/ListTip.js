import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';

import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  StatusBar,
  SafeAreaView,
  TextInput,
} from 'react-native';
import style from './styles';
import {Icon} from 'react-native-elements';
import {listAllTip} from '../../../api';
import {Button, List} from 'react-native-paper';
import {useSelector} from 'react-redux';

const Data = [
  {
    title: 'Appoinssddatments',
    icon: 'flight-takeoff',
  },
  {
    title: 'dss',
    icon: 'flight-takeoff',
  },
];
export default function ListTip(props) {
  const [allListTip, setAllListTip] = useState([]);
  const token = useSelector(state => state.auth);
  const navigation = useNavigation();

  const fetchListtip = () => {
    listAllTip(token)
      .then(res => {
        setAllListTip(res.data);
        console.log('Test data store Tip ', res);
        console.log('data Tip store data=============== ', res.data);
        console.log('Tip tp=============== ', res.data.Topic);
        // console.log('Name Tip=============== ', res.data.Sex);
      })
      .catch(error => {
        if (error.res) {
          console.log('error', error);
        } else {
          console.log('connect Error!!!');
        }
      });
  };
  useEffect(() => {
    const unsubcrible = navigation.addListener('focus', () => {
      fetchListtip();
    });
    return unsubcrible;
  }, [navigation]);

  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 200,
            backgroundColor: '#fbd3d3',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ImageBackground
            source={require('../../../asset/sleep2.jpg')}
            resizeMode="cover"
            style={style.sizeBg}
          />
        </View>
        <ScrollView
          style={{backgroundColor: '#fff', flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#ad899d',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomLeftRadius: 96,
                borderBottomRightRadius: 96,
                height: 48,
                width: '100%',
                marginBottom: 32,
                flexDirection: 'row',
              }}>
              <Text style={style.headkaigaodown}>List All tip</Text>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('AddTip')}>
                <Icon
                  name="ios-add-circle"
                  type="ionicon"
                  color="#fff"
                  size={32}
                  style={{marginRight: 8}}
                />
              </TouchableOpacity>
            </View>
            {allListTip.map((item, index) => (
              <List.Item
                title={item.Topic}
                style={{
                  backgroundColor: 'white',
                  borderColor: 'black',
                  borderBottomWidth: 1,
                  width: 296,
                  marginBottom: 8,
                }}
                left={props => (
                  <Icon
                    name="folder"
                    type="material-community"
                    style={{margin: 8}}
                  />
                )}
                onPress={() =>
                  props.navigation.navigate('InfoTip', {item, index})
                }
              />
              // <List.Item
              //   title="bnm,.jkl"
              //   style={{
              //     backgroundColor: 'red',
              //     borderColor: 'black',
              //     borderBottomWidth: 1,
              //     width: 296,
              //     marginBottom: 8,
              //   }}
              //   left={props => (
              //     <Icon
              //       name="folder"
              //       type="material-community"
              //       style={{margin: 8}}
              //     />
              //   )}
              //   onPress={() => props.navigation.navigate('InfoTip')}
              // />
            ))}
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
