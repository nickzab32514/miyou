import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, Text, SafeAreaView} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import {
  check,
  request,
  PERMISSIONS,
  RESULTS,
  openSettings,
} from 'react-native-permissions';
import {Button} from 'react-native-paper';
import {AllPet} from '../../../api';
console.disableYellowBox = true;
export default function Allmap(props) {
  const [pet, setPet] = useState([]);
  const [coordinate, setCoordinate] = useState({
    latitude: 0,
    longitude: 0,
  });

  const MapViewRef = useRef();

  useEffect(() => {
    console.log('MapViewRef new: ', MapViewRef);
    check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
      if (result === RESULTS.GRANTED) {
        callLocation(position => {
          console.log('position: ', position);
          setCoordinate(position.coords);
          MapViewRef.current.animateCamera({
            center: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            },
          });
        });
      }
      if (result === RESULTS.DENIED) {
        request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
          if (result === RESULTS.GRANTED) {
            callLocation(position => {
              setCoordinate(position.coords);
              MapViewRef.current.animateCamera({
                center: {
                  latitude: position.coords.latitude,
                  logitude: position.coords.longitude,
                },
              });
            });
          } else if (result === RESULTS.DENIED) {
            alert('Please accept');
          } else if (result === RESULTS.BLOCKED) {
            alert('Thank you');
          }
        });
        return;
      }
      if (result === RESULTS.BLOCKED) {
        alert('...');
      }
    });
    fetchData();
  }, []);

  const fetchData = () => {
    AllPet()
      .then(res => {
        console.log('res.data: ', res.data);
        setPet(res.data);
      })
      .catch(err => {
        console.log('err : ', err);
      });
  };
  const callLocation = callback => {
    Geolocation.getCurrentPosition(
      position => {
        callback(position);
      },
      error => {
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  };
  const ButtonBack = () => (
    <Button
      icon="account-arrow-right"
      color="black"
      mode="contained"
      onPress={() => alert('Ahhhhhhhhhhhhhhhhhhh')}>
      Back
    </Button>
  );

  if (pet.length > 0) {
    const test = pet[0].Latitude;
    console.log('pett13123213: ', +test);
    return (
      <View style={{flex: 1}}>
        <SafeAreaView>
          <View
            style={{
              height: '100%',
              width: '100%',
            }}>
            <MapView
              ref={MapViewRef}
              provider={PROVIDER_GOOGLE}
              style={styles.map}
              region={{
                latitude: coordinate.latitude,
                longitude: coordinate.longitude,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}>
              {pet.map((item, index) => {
                return (
                  <Marker
                    image={require('../../../asset/animals.png')}
                    coordinate={{
                      latitude: +item.Latitude,
                      longitude: +item.Longitude,
                    }}>
                    <Callout>
                      <Text>{item.Name}</Text>
                    </Callout>
                  </Marker>
                );
              })}
              <Marker
                image={require('../../../asset/start.png')}
                coordinate={coordinate}
              />
            </MapView>
            <View style={styles.cardContainer}>
              <Button
                icon="account-arrow-right"
                color="#f2b9c7"
                mode="contained"
                onPress={() => props.navigation.navigate('TopTab')}>
                Back
              </Button>
            </View>
          </View>
        </SafeAreaView>
        <ButtonBack />
      </View>
    );
  } else {
    return <View />;
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    height: 150,
    width: 300,
    padding: 24,
    marginLeft: 40,
    marginTop: 560,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 16,
    alignSelf: 'center',
  },
});
