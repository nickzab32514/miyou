import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  ScrollView,
  StatusBar,
  SafeAreaView,
  Image,
} from 'react-native';
import Modal from 'react-native-modal';
import {useSelector} from 'react-redux';
import style from './styles';
import {Icon} from 'react-native-elements';
import {Button, List} from 'react-native-paper';
import {showProfile, logout} from '../../../api';

export default function Profile(props) {
  const [modalVisible, setmodalVisible] = useState(false);
  const setModalVisible = visible => setmodalVisible(visible);
  const token = useSelector(state => state.auth);

  const [userData, setUserData] = useState([]);

  const handdleLogout = () => {
    logout(token)
      .then(res => {
        console.log('res-->', res);
      })
      .catch(err => {
        console.log('Error====== : ', err);
      });
    props.navigation.navigate('Login');
  };
  const fetchProfile = () => {
    showProfile(token)
      .then(res => {
        setUserData(res.data);
        console.log('res---<>', res.data);
      })
      .catch(err => {
        console.log('Error====== : ', err);
      });
  };

  useEffect(() => {
    fetchProfile();
  }, []);

  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 300,
            backgroundColor: '#fff',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={require('../../../asset/smile.png')}
            resizeMode="cover"
            style={style.sizeBg}
          />
          <View style={style.flexrowpro}>
            <Text style={style.textpro}>{userData.fistName}</Text>
            <Text style={style.textpro}>{userData.lastName}</Text>
          </View>
          <View style={style.flexrow}>
            <Text style={style.textUser}>@</Text>
            <Text style={style.textUser}>{userData.username}</Text>
          </View>
        </View>
        <ScrollView
          style={{backgroundColor: '#e3d3cc', flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#ad899d',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomLeftRadius: 96,
                borderBottomRightRadius: 96,
                height: 48,
                width: '100%',
                marginBottom: 32,
              }}>
              <Text style={style.headkaigaodown}>Profile</Text>
            </View>
            <List.Item
              title="History add list tip"
              style={{
                backgroundColor: 'white',
                borderColor: 'black',
                width: 296,
                marginBottom: 8,
              }}
              left={props => (
                <Icon
                  name="av-timer"
                  type="material-community"
                  style={{margin: 8}}
                />
              )}
              onPress={() => props.navigation.navigate('HistoryTip')}
            />
            <List.Item
              title="History add list pet"
              style={{
                backgroundColor: 'white',
                borderColor: 'black',
                width: 296,
                marginBottom: 8,
              }}
              left={props => (
                <Icon
                  name="av-timer"
                  type="material-community"
                  style={{margin: 8}}
                />
              )}
              onPress={() => props.navigation.navigate('HistoryPet')}
            />
            <List.Item
              title="History Accept"
              style={{
                backgroundColor: 'white',
                borderColor: 'black',
                width: 296,
                marginBottom: 8,
              }}
              left={props => (
                <Icon
                  name="av-timer"
                  type="material-community"
                  style={{margin: 8}}
                />
              )}
              onPress={() => props.navigation.navigate('HistoryAccept')}
            />
            <List.Item
              title="Log out"
              style={{
                backgroundColor: 'white',
                borderColor: 'black',
                width: 296,
                marginBottom: 24,
              }}
              left={props => (
                <Icon name="logout" type="antdesign" style={{margin: 8}} />
              )}
              onPress={() => setModalVisible(true)}
            />
          </View>
          <Modal
            animationType="slide"
            transparent={false}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View
              style={{
                marginTop: 22,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: 36, marginBottom: 32, fontWeight: '900'}}>
                Successful
              </Text>
              <Image
                style={{width: 200, height: 200, marginBottom: 24}}
                source={require('../../../asset/check.png')}
              />
              <Button mode="text " onPress={handdleLogout}>
                <Text style={{color: 'black'}}>close</Text>
              </Button>
            </View>
          </Modal>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
