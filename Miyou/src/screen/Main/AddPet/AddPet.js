import React, {useState, useRef, useEffect} from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  StatusBar,
  SafeAreaView,
  TextInput,
  StyleSheet,
} from 'react-native';
import style from './styles';
import {Icon} from 'react-native-elements';
import {Button} from 'react-native-paper';
import {IndexPath, Select, SelectItem} from '@ui-kitten/components';
import {useSelector} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import {TouchableOpacity} from 'react-native-gesture-handler';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import {
  check,
  request,
  PERMISSIONS,
  RESULTS,
  openSettings,
} from 'react-native-permissions';

import {createOnePet, UpdateImage} from '../../../api/index';

export default function AddPet(props) {
  const token = useSelector(state => state.auth);

  const [name, setName] = useState('');
  const [gene, setGene] = useState('');
  const [address, setAddress] = useState('');
  const [age, setAge] = useState('');
  const [contractNumber, setContractNumber] = useState('');
  const [desease, setDesease] = useState('');
  const [description, setDescription] = useState('');
  const [type, setType] = useState('');

  const renderOption = title => <SelectItem title={title} />;
  const data = ['male', 'female'];
  const [selectedIndex, setSelectedIndex] = useState(new IndexPath(0));
  const displayValue = data[selectedIndex.row];

  const [photo, setPhoto] = useState('');
  const [dataPhoto, setDataPhoto] = useState({});

  const MapViewRef = useRef();
  const [location, setLocation] = useState('');
  const [coordinate, setCoordinate] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
  });

  const addPhoto = () => {
    const options = {
      title: 'Select Photo',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const uri = response.uri;
        const type = response.type;
        const name = response.fileName;
        const source = {
          uri,
          type,
          name,
        };
        setPhoto(response);
        setDataPhoto(source);
      }
    });
  };
  const cloudinaryUpload = () => {
    const data = new FormData();
    data.append('file', dataPhoto);
    data.append('upload_preset', 'hlw7nu56');
    data.append('cloud_name', 'dhru2dpkz');
    fetch('https://api.cloudinary.com/v1_1/dhru2dpkz/image/upload', {
      method: 'post',
      body: data,
    })
      .then(res => res.json())
      .then(data => {
        console.log('url===============>', data.secure_url);
        addPet(data.secure_url);
      })
      .catch(err => {
        alert('An Error Occured While Uploading');
        console.log('error------------------>', err);
      });
  };

  const addPet = photo => {
    let data = {
      name: name,
      gene: gene,
      type: type,
      address: address,
      sex: displayValue,
      age: age,
      contractNumber: contractNumber,
      description: description,
      desease: desease,
      image: photo,
      latitude: coordinate.latitude.toString(),
      longitude: coordinate.longitude.toString(),
    };
    console.log('firstname==>', displayValue);

    createOnePet(data, token)
      .then(response => {
        const petId = response.data.ID;
        console.log('Test PEt--> ', response.data);
        props.navigation.navigate('infoPet');
      })

      .catch(error => {
        console.log('eror', response);
      });
    props.navigation.navigate('TopTab');
  };

  const callLocation = callback => {
    Geolocation.getCurrentPosition(
      position => {
        callback(position);
      },
      error => {
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  };

  useEffect(() => {
    console.log('geolocation------->', coordinate);
    check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
      if (result === RESULTS.GRANTED) {
        callLocation(position => {
          setCoordinate(position.coords);
          MapViewRef.current.animateCamera({
            center: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            },
          });
        });
      }
      if (result === RESULTS.DENIED) {
        request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
          if (result === RESULTS.GRANTED) {
            callLocation(position => {
              setCoordinate(position.coords);
              MapViewRef.current.animateCamera({
                center: {
                  latitude: position.coords.latitude,
                  longitude: position.coords.longitude,
                },
              });
            });
          } else if (result === RESULTS.DENIED) {
            alert('คูณปฎิเสทในการลองรับการใช้งานค้นหาแผนที่');
          } else if (result === RESULTS.BLOCKED) {
            alert('คุณถูกบล๊อคเนื่องจากปฎิเสทการใช้งานอีกครั้งแมป');
          }
        });
        return;
      }
      if (result === RESULTS.BLOCKED) {
        alert('คุณถูกบล๊อคเนื่องจากปฎิเสทการใช้งานอีกครั้งแมป');
      }
    });
  }, []);

  return (
    <View style={style.sizePage} contentContainerStyle={{flexGrow: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <SafeAreaView style={style.sizePage}>
        <View
          style={{
            height: 56,
            backgroundColor: '#4D70EB',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity onPress={() => props.navigation.navigate('TopTab')}>
            <Icon
              name="ios-arrow-dropleft"
              type="ionicon"
              color="#fff"
              style={{marginLeft: 32, marginTop: 32}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 150,
            backgroundColor: '#4D70EB',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TouchableOpacity>
            {photo === '' || photo === undefined ? (
              <TouchableOpacity onPress={addPhoto}>
                <Image
                  resizeMode="cover"
                  source={require('../../../asset/cameDd.jpg')}
                  style={style.tinyLogo}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={addPhoto}>
                <Image source={photo} style={style.tinyLogo} />
              </TouchableOpacity>
            )}
          </TouchableOpacity>
        </View>
        <ScrollView
          style={{flex: 1, backgroundColor: '#e1e5f7'}}
          contentContainerStyle={{flexGrow: 1}}>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <View
              style={{
                backgroundColor: '#92B0E9',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomLeftRadius: 96,
                borderBottomRightRadius: 96,
                height: 48,
                width: '100%',
                marginBottom: 32,
              }}>
              <Text style={style.headkaigaodown}>Detail Add Information</Text>
            </View>
            <View style={style.flexrowAddMap}>
              <Icon
                name="map-legend"
                type="material-community"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Map
              </Text>
            </View>
            <View />
            <View
              style={{
                height: 300,
                width: 320,
                margin: 20,
              }}>
              <MapView
                ref={MapViewRef}
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                region={{
                  latitude: coordinate.latitude,
                  longitude: coordinate.longitude,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
                }}>
                <Marker
                  draggable
                  coordinate={coordinate}
                  onDragEnd={event =>
                    setCoordinate(event.nativeEvent.coordinate)
                  }
                />
              </MapView>
            </View>
            <View style={style.flexrowL}>
              <Icon
                name="account-circle-outline"
                type="material-community"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Name
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input name'}
                mode="Flat "
                label="name"
                value={name}
                onChangeText={value => setName(value)}
              />
            </View>
            <View style={style.flexrowL}>
              <Icon
                name="dna"
                type="fontisto"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Gene
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input gene'}
                mode="Flat "
                label="gene"
                value={gene}
                onChangeText={value => setGene(value)}
              />
            </View>
            <View style={style.flexrowSex}>
              <Icon
                name="intersex"
                type="fontisto"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Sex
              </Text>
            </View>
            <View style={style.flexrow}>
              <Select
                style={style.selectsex}
                placeholder="Please selete sex"
                value={displayValue}
                selectedIndex={selectedIndex}
                onSelect={index => setSelectedIndex(index)}>
                {data.map(renderOption)}
              </Select>
            </View>
            <View style={style.flexrowPh}>
              <Icon
                name="hourglass-start"
                type="fontisto"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Age
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please  input age'}
                mode="Flat "
                label="Age"
                value={age}
                onChangeText={value => setAge(value)}
              />
            </View>
            <View style={style.flexrowL}>
              <Icon
                name="baidu"
                type="entypo"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Type
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input disease'}
                mode="Flat "
                label="disease"
                value={type}
                onChangeText={value => setType(value)}
              />
            </View>
            <View style={style.flexrowDes}>
              <Icon
                name="map-marker-radius"
                type="material-community"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Description style
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'style'}
                mode="Flat "
                multiline
                label="Please input dtyle"
                value={description}
                onChangeText={value => setDescription(value)}
              />
            </View>
            <View style={style.flexrowL}>
              <Icon
                name="hubspot"
                type="material-community"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Disease
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input disease'}
                mode="Flat "
                label="disease"
                value={desease}
                onChangeText={value => setDesease(value)}
              />
            </View>
            <View style={style.flexrowConNum}>
              <Icon
                name="phone-classic"
                type="material-community"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Contract number
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input  Contract number'}
                mode="Flat "
                label=" ContractNumber"
                value={contractNumber}
                onChangeText={value => setContractNumber(value)}
              />
            </View>
            <View style={style.flexrowAdd}>
              <Icon
                name="map-marker-radius"
                type="material-community"
                color="#597fbf"
                style={{marginRight: 8}}
              />
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: '#597fbf'}}>
                Address
              </Text>
            </View>
            <View style={style.flexrow}>
              <TextInput
                style={style.inputText}
                placeholder={'Please input address'}
                mode="Flat "
                multiline
                label="address"
                value={address}
                onChangeText={value => setAddress(value)}
              />
            </View>
          </View>
          <View style={style.flexrowS}>
            <Button
              style={style.buttonSubmit}
              mode="Contained"
              onPress={cloudinaryUpload}>
              <Text style={style.whitefont}>Submit</Text>
            </Button>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
