import React from 'react';
import Main from './router/Main';
import 'react-native-gesture-handler';
import store from './store';
import {Provider} from 'react-redux';
// import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import * as eva from '@eva-design/eva';
import {ApplicationProvider} from '@ui-kitten/components';

export default function App() {
  return (
    <Provider store={store}>
      <ApplicationProvider {...eva} theme={eva.light}>
        <Main />
      </ApplicationProvider>
    </Provider>
  );
}
