import React, {Component} from 'react';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import 'react-native-gesture-handler';

import {createStackNavigator} from '@react-navigation/stack';
import TopTab from './TopTab';
import Login from '../screen/Auth/Login/Login';
import Register from '../screen/Auth/Register/Register';
import AddPet from '../screen/Main/AddPet/AddPet';
import AddTip from '../screen/Main/AddTip/AddTip';
import ListPet from '../screen/Main/ListPet/ListPet';
import AcceptList from '../screen/Main/ListPet/AcceptList';
import NotAcceptList from '../screen/Main/ListPet/NotAcceptList';
import All from '../screen/Main/ListPet/All';
import InfoPet from '../screen/Main/InfoPet/InfoPet';
import InfoTip from '../screen/Main/InfoTip/InfoTip';
import HistoryTip from '../screen/Main/HistoryTip/HistoryTip';
import HistoryPet from '../screen/Main/HistoryAddPet/HistoryPet';
import HistoryAccept from '../screen/Main/HistoryAccept/HistoryAccept';
import InfoDelete from '../screen/Main/InfoDeleletPet/InfoDelete';
import Allmap from '../screen/Main/Allmap/Allmap';

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'rgb(227, 211, 204)',
  },
};
const Stack = createStackNavigator();

export default function Main() {
  return (
    <NavigationContainer theme={MyTheme}>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="TopTab" component={TopTab} />
        <Stack.Screen name="AcceptList" component={AcceptList} />
        <Stack.Screen name="NotAcceptList" component={NotAcceptList} />
        <Stack.Screen name="All" component={All} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="AddPet" component={AddPet} />
        <Stack.Screen name="AddTip" component={AddTip} />
        <Stack.Screen name="ListPet" component={ListPet} />
        <Stack.Screen name="InfoPet" component={InfoPet} />
        <Stack.Screen name="InfoTip" component={InfoTip} />
        <Stack.Screen name="HistoryPet" component={HistoryPet} />
        <Stack.Screen name="HistoryAccept" component={HistoryAccept} />
        <Stack.Screen name="HistoryTip" component={HistoryTip} />
        <Stack.Screen name="InfoDelete" component={InfoDelete} />
        <Stack.Screen name="Allmap" component={Allmap} />
        {/* <Stack.Screen name="Logout" component={Logout} />
        <Stack.Screen name="Allmap" component={Allmap} />
        <Stack.Screen name="MyPet" component={MyPet} />
        <Stack.Screen name="EditProfile" component={EditProfile} />
        <Stack.Screen name="EditPet" component={EditPet} />
         */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
