import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Profile from '../screen/Main/Profile/Profile';
import ListTip from '../screen/Main/ListTip/ListTip';
import ListPet from '../screen/Main/ListPet/ListPet';
import {View} from 'react-native';
// import colors from 'src/themes/colors';
// import Icon from 'react-native-vector-icons/Ionicons';

const Tab = createMaterialTopTabNavigator();

export default function TopTab() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        labelStyle: {fontSize: 16},
        style: {backgroundColor: '#c5999d'},
        activeTintColor: '#fff',
        inactiveTintColor: '#fff',
        pressColor: 'rgb(227, 211, 204)',
      }}
      initialRouteName="ListPet">
      <Tab.Screen
        name="ListTip"
        component={ListTip}
        options={{
          tabBarLabel: 'List Tip',
        }}
      />
      <Tab.Screen
        name="ListPet"
        component={ListPet}
        options={{
          tabBarLabel: 'List Pet',
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
        }}
      />
    </Tab.Navigator>
  );
}
