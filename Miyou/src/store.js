import {createStore} from 'redux';
import comblineReducer from './reducer/comblineReducer';
import React from 'react';

const store = createStore(comblineReducer);

export default store;

export const StoreContext = React.createContext(store);
